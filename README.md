# TynixCloud Public Java SDK
***
## | `Author-Contacts:`
* **[ Discord ]** - ~~_https://discord.gg/SWZ2cPGnBT_~~
* **[ ВКонтакте ]** - _~~https://vk.com/kaytato~~_
* **[ Telegram ]** - _~~https://t.me/kaytatochannel~~_
***
## | `TynixSdk:`
Все запросы отправляются через класс `ru.tynixcloud.sdk.TynixSdk`, пример получения объекта класса:

```java
TynixSdk sdk = new TynixSdk("you-token"); // ОБЯЗАТЕЛЬНО: Измените токен на свой.
sdk.setDebug(true); // Нужен ли вам вывод всех операций в консоль?
```
***
## | `Requests:`
***
### `OnlineRequest:`
Получение общего онлайна проекта:
```java
sdk.GLOBAL_ONLINE_REQUEST.getTotalOnlineResponse();
```
Возможные **ошибки** при использовании: TokenHasExpiredException, ParamNotFoundException
***
### `GroupsRequest:`
Получение группы по её уровню:
```java
sdk.GROUPS_REQUEST.getGroupResponse(100);
```
Возможные **ошибки** при использовании: TokenHasExpiredException, ParamNotFoundException
***
### `PlayerGroupRequest:`
Получение id группы игрока:
```java
sdk.PLAYER_GROUP_REQUEST.getPlayerGroupResponse("Frotter_");
```
Возможные **ошибки** при использовании: TokenHasExpiredException, ParamNotFoundException, PlayerNotFoundException
***
### `PlayerInformationRequest:`
Получение основной информации об игроке:
```java
sdk.PLAYER_INFORMATION_REQUEST.getPlayerInformationResponse("Frotter_");
```
Возможные **ошибки** при использовании: TokenHasExpiredException, ParamNotFoundException, PlayerNotFoundException
***
### `PlayerNameRequest:`
Получение никнейма игрока, по его id:
```java
sdk.PLAYER_NAME_REQUEST.getPlayerNameResponse(2).getName();
```
Возможные **ошибки** при использовании: TokenHasExpiredException, ParamNotFoundException, PlayerNotFoundException
***
### `PlayerGuildRequest:`
Получение названия гильдии игрока, по его нику:
```java
sdk.PLAYER_GUILD_REQUEST.getPlayerGuildResponse("Frotter_").getGuildName();
```
Возможные **ошибки** при использовании: TokenHasExpiredException, ParamNotFoundException, PlayerNotFoundException
***
## | `Ошибки`
Later..
