package ru.tynixcloud.sdk.exception;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class ParamNotFoundException extends Exception {

    private String param;

    public ParamNotFoundException(String param) {
        this.param = param;
    }

    @Override
    public String getLocalizedMessage() {
        return "Ошибка, не передан параметр " + param;
    }

    @Override
    public String getMessage() {
        return "Im sorry, not found param " + param;
    }
}
