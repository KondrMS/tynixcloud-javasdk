package ru.tynixcloud.sdk.request.player;

import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import ru.tynixcloud.sdk.TynixSdk;
import ru.tynixcloud.sdk.request.BaseRequest;
import ru.tynixcloud.sdk.utility.HttpParam;
import ru.tynixcloud.sdk.utility.JsonUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class PlayerGuildRequest extends BaseRequest {
    public PlayerGuildRequest(@NonNull TynixSdk sdk) {
        super(sdk, "player/guild");
    }

    public GetPlayerGuildResponse getPlayerGuildResponse(String playerName) throws ExecutionException, InterruptedException, IOException {
        JsonObject response = JsonUtil.parse(get(request, new HttpParam("name", playerName)))
                .getAsJsonObject()

                .get("response")
                .getAsJsonObject();
        String name = response.get("name").getAsString();
        String guildName = response.get("guildName").getAsString();
        String guildLeader = response.get("guildLeader").getAsString();
        int rank = response.get("rank").getAsInt();
        int guildMembers = response.get("guildMembers").getAsInt();
        int guildMaxMembers = response.get("guildMaxMembers").getAsInt();
        int guildCoins = response.get("guildCoins").getAsInt();
        int guildGolds = response.get("guildGolds").getAsInt();

        return new GetPlayerGuildResponse(name, guildName, guildLeader, rank, guildMembers, guildMaxMembers, guildCoins, guildGolds);
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    public static class GetPlayerGuildResponse {

        String name;
        String guildName;
        String guildLeader;
        int rank;
        int guildMembers;
        int guildMaxMembers;
        int guildCoins;
        int guildGolds;


    }
}