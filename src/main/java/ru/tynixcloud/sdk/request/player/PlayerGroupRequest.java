package ru.tynixcloud.sdk.request.player;

import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import ru.tynixcloud.sdk.TynixSdk;
import ru.tynixcloud.sdk.request.BaseRequest;
import ru.tynixcloud.sdk.utility.HttpParam;
import ru.tynixcloud.sdk.utility.JsonUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
/**
 * Created by kaytato, for tynixcloud.
 * @VK: https://vk.com/kaytato
 */
public class PlayerGroupRequest extends BaseRequest {


    public PlayerGroupRequest(@NonNull TynixSdk sdk) {
        super(sdk, "player/group");
    }

    public GetPlayerGroupResponse getPlayerGroupResponse(@NonNull String playerName) throws ExecutionException, InterruptedException, IOException {
        JsonObject response = JsonUtil.parse(get(request, new HttpParam("name", playerName)))
                .getAsJsonObject()

                .get("response")
                .getAsJsonObject();
        String name = response.get("name").getAsString();
        int groupId = response.get("groupId").getAsInt();

        return new GetPlayerGroupResponse(name, groupId);
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    public static class GetPlayerGroupResponse {

        //int code;
        String name;
        int groupId;


    }

}
